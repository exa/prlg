member(X, [X|_]).
member(X, [_|T]) :- member(X,T).

append([], [], []).
append([], [H|T], [H|T]).
append([X|T], Y, [X|TY]) :- append(T,Y,TY).

list([]).
list([_|_]).

:- op(700, xfx, is),
   op(700, xfx, <),
   op(700, xfx, =<),
   op(700, xfx, >),
   op(700, xfx, >=),
   op(700, xfx, =\=),
   op(700, xfx, =:=),
   op(500, yfx, +),
   op(500, yfx, -),
   op(500, yfx, /\),
   op(500, yfx, \/),
   op(500, yfx, xor),
   op(400, yfx, *),
   op(400, yfx, /),
   op(400, yfx, div),
   op(400, yfx, mod),
   op(400, yfx, <<),
   op(400, yfx, >>),
   op(200, fy, -),
   op(200, fy, +).

X is A :- number(A), !, X=A.
X is V :- var(V), !, stop('variable in arithmetics').
R is Ax + Bx :- !, A is Ax, B is Bx, int2_add(A, B, R).
R is Ax - Bx :- !, A is Ax, B is Bx, int2_sub(A, B, R).
R is Ax * Bx :- !, A is Ax, B is Bx, int2_mul(A, B, R).
R is Ax / Bx :- !, A is Ax, B is Bx, int2_div(A, B, R).
R is Ax div Bx :- !, R is Ax / Bx.
R is Ax mod Bx :- !, A is Ax, B is Bx, int2_mod(A, B, R).
R is (+Ax) :- !, A is Ax, int1_abs(A, R).
R is (-Ax) :- !, A is Ax, int1_neg(A, R).
_ is _ :- stop('arithmetics needs numbers').

Ax =:= Bx :- A is Ax, B is Bx, int2p_eq(A,B).
Ax =\= Bx :- A is Ax, B is Bx, int2p_neq(A,B).
Ax < Bx :- A is Ax, B is Bx, int2p_lt(A,B).
Ax =< Bx :- A is Ax, B is Bx, int2p_leq(A,B).
Ax > Bx :- A is Ax, B is Bx, int2p_lt(B,A).
Ax >= Bx :- A is Ax, B is Bx, int2p_leq(B,A).
zero(Ax) :- A is Ax, int1p_zero(A).

gcd(X,Y,R) :- Y > X, !, gcd(Y,X,R).
gcd(X,Y,R) :- zero(Y), !, R=X.
gcd(X,Y,R) :- X1 is X mod Y, gcd(Y,X1,R).

lcm(X,Y,R) :- gcd(X,Y,GCD), R is X*(Y/GCD).

:- op(1200, xfx, -->).

sequence([], SameState, SameState) :- !.
sequence(Tokens, ParsedList, Rest) :-
  append(Tokens, Rest, ParsedList).

load_expansion((X, L) --> Y, Xp :- (Yp, Lp)) :- !,
  expand_phrasecall(X, Xp, S0, S),
  expand_phrase(Y, Yp, S0, S),
  expand_phrase(L, Lp, S, _).

load_expansion(X --> Y, Xp :- Yp) :- !,
  expand_phrasecall(X, Xp, S0, S),
  expand_phrase(Y, Yp, S0, S).

expand_phrase((A, B), (Ap, Bp), S0, S) :- !,
  expand_phrase(A, Ap, S0, S1),
  expand_phrase(B, Bp, S1, S).
expand_phrase((A; B), (Ap; Bp), S0, S) :- !,
  expand_phrase(A, Ap, S0, S),
  expand_phrase(B, Bp, S0, S).
expand_phrase(L, sequence(L, S0, S), S0, S) :- list(L), !.
expand_phrase({X}, X, S, S) :- !.
expand_phrase(!, !, S, S) :- !.
expand_phrase(X, Xp, S0, S) :- expand_phrasecall(X, Xp, S0, S).

expand_phrasecall(X, Xp, S0, S) :-
  atom(X), !,
  struct(Xp, X, [S0, S]).
expand_phrasecall(X, Xp, S0, S) :- !,
  struct(X, Id, Args),
  append(Args, [S0, S], Args1),
  struct(Xp, Id, Args1).

phrase(X, S0, S) :- expand_phrasecall(X, Xp, S0, S), call(Xp).
