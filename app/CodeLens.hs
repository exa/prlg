{-# LANGUAGE TemplateHaskell #-}

module CodeLens where

import Code
import Lens.Micro.TH

makeLenses ''Cho

makeLenses ''Interp
