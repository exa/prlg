module Constant where

data Constant
  = Atom Int
  | Number Int
  | Str String
  deriving (Show, Eq, Ord)
