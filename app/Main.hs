module Main where

import Frontend (interpreter)
import System.Console.Haskeline (Settings(..), defaultSettings, runInputT)
import System.Environment (lookupEnv)

main :: IO ()
main = do
  hf <- lookupEnv "HOME"
  runInputT
    defaultSettings {historyFile = fmap (++ "/.prlg_history") hf}
    interpreter
