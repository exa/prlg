module Env where

import Code (Id(..), InterpFn, PrlgEnv)
import CodeLens
import IR (StrTable, strtablize)
import Lens.Micro.Mtl

withStrTable :: (StrTable -> (StrTable, a)) -> Env.PrlgEnv a
withStrTable f = do
  (st', x) <- f <$> use strtable
  strtable .= st'
  return x

findStruct :: String -> Int -> Env.PrlgEnv Id
findStruct str arity = do
  stri <- findAtom str
  return Id {str = stri, arity = arity}

findAtom :: String -> Env.PrlgEnv Int
findAtom = withStrTable . flip strtablize

type PrlgEnv a = Code.PrlgEnv a

prlgError :: String -> InterpFn
prlgError = pure . pure . Left
